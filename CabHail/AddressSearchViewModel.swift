//  Copyright © 2017 Erick Vavretchek. All rights reserved.

import Foundation

protocol AddressSearchViewControllerProtocol: class {
  func startActivityIndicator()
  func stopActivityIndicator()
  func dismissScreen()
  func reloadData()
}

final class AddressSearchViewModel {
  
  private unowned let viewController: AddressSearchViewControllerProtocol
  private var timer: Timer?
  private var task: URLSessionTask?
  
  init(viewController: AddressSearchViewControllerProtocol) {
    self.viewController = viewController
  }
  
  func search(for text: String) {
    viewController.startActivityIndicator()
    timer?.invalidate()
    timer = Timer.scheduledTimer(withTimeInterval: 1, repeats: false) { [weak self] _ in
      self?.get(query: text)
      self?.viewController.stopActivityIndicator()
    }
  }
  
  func cancelRequest() {
    timer?.invalidate()
    task?.cancel()
    viewController.stopActivityIndicator()
    viewController.dismissScreen()
  }
  
  func get(query: String) {
    // Work in progress
    let url = URL(string: "https://api.foursquare.com/v2/venues/search?v=20161016&ll=-33.822899%2C%20151.196494&intent=checkin&client_id=WGKIFWDXZ1UJF5URUEG1AOVCVHBS5YECWLFYAYO2PDRTU155&client_secret=PKAIXZQXS0BOOLWV1H5HXZQVJ4E5SN4KI0QNORUUECOQFC5L&limit=10&query=\(query)")
    task?.cancel()
    task = URLSession.shared.dataTask(with: url!, completionHandler: { [weak self] (data, response, error) in
      if (error != nil) {
        print("error")
      } else {
        do{
          let json = try JSONSerialization.jsonObject(with: data!, options:.allowFragments) as! [String : AnyObject]
          let places = (json["response"]!["venues"]! as! [[String : AnyObject]])
          for place in places {
            print(place["name"] as! String)
            print(place["location"]!["city"] as? String)
            print(place["location"]!["distance"] as! Int)
            print("-----------------------------------------")
          }
          DispatchQueue.main.async {
            self?.viewController.stopActivityIndicator()
            self?.viewController.reloadData()
          }
        } catch let error as NSError{
          print(error)
        }
      }
    })
    task?.resume()
  }
}

