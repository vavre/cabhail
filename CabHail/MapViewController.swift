//  Copyright © 2017 Erick Vavretchek. All rights reserved.

import UIKit
import MapKit

final class MapViewController: UIViewController {
  
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var mapView: MKMapView!
  
  lazy var viewModel: MapViewModel = MapViewModel(viewController: self)
  
  // MARK: UIViewController
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupUI()
    
    
    
  }
  
  // MARK: Actions
  
  @objc func addressTapAction() {
    viewModel.addressScreenRequest()
  }
  
  // MARK: Private
  
  private func setupUI() {
    
    let addressTap = UITapGestureRecognizer(target: self, action: #selector(addressTapAction))
    addressLabel.addGestureRecognizer(addressTap)
    
    addressLabel.layer.borderColor = UIColor.lightGray.cgColor
    addressLabel.backgroundColor = UIColor.white
    addressLabel.layer.borderWidth = 1
    addressLabel.layer.cornerRadius = 4
    addressLabel.clipsToBounds = true
    
  }
}

extension MapViewController: MapViewControllerProtocol {
  
  func displayAddressScreen() {
    let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
    let viewController = storyboard.instantiateViewController(withIdentifier: "AddressViewController")
    present(viewController, animated: true, completion: nil)
  }
}
