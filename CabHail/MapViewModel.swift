//  Copyright © 2017 Erick Vavretchek. All rights reserved.

import Foundation

protocol MapViewControllerProtocol: class {
  func displayAddressScreen()
}

final class MapViewModel {
  
  private unowned let viewController: MapViewControllerProtocol
  
  init(viewController: MapViewControllerProtocol) {
    self.viewController = viewController
  }
  
  func addressScreenRequest() {
    viewController.displayAddressScreen()
  }
}
