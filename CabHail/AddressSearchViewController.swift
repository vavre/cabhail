//  Copyright © 2017 Erick Vavretchek. All rights reserved.

import UIKit

class AddressSearchViewController: UIViewController {
  
  
  @IBOutlet weak var searchBar: UISearchBar!
  @IBOutlet weak var cancelButton: UIButton!
  @IBOutlet weak var tableView: UITableView!
  
  lazy var viewModel: AddressSearchViewModel = AddressSearchViewModel(viewController: self)
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.tableFooterView = UIView()
    searchBar.delegate = self
    
  }
  
  @IBAction func cancelTapAction(_ sender: Any) {
    viewModel.cancelRequest()
  }
}

extension AddressSearchViewController: AddressSearchViewControllerProtocol {
  
  func startActivityIndicator() {
    UIApplication.shared.isNetworkActivityIndicatorVisible = true
  }
  func stopActivityIndicator() {
    UIApplication.shared.isNetworkActivityIndicatorVisible = false
  }
}

extension AddressSearchViewController: UISearchBarDelegate {
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    viewModel.search(for: searchText)
  }
  
  func dismissScreen() {
    dismiss(animated: true)
  }
  
  func reloadData() {
    tableView.reloadData()
  }
}
